import com.pm.ValidateAspect
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator

// Place your Spring DSL code here
//import pm

beans = {
    //ValidatedAspect(ValidateAspect)
    validateAspect(ValidateAspect)
/*
    autoProxyCreator(AnnotationAwareAspectJAutoProxyCreator) {
        proxyTargetClass = true
    }*/
}
