package com.pm

import grails.validation.ValidationException
import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes

import javax.servlet.http.HttpServletRequest


@Aspect
class ValidateAspect {

    @Around("@annotation(com.pm.Validate)")
    public Object preValidate(ProceedingJoinPoint point) throws ValidationException{
        // validate
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        println "parameters  ${point.getArgs()}"
        return new ResponseEntity(HttpStatus.FORBIDDEN);

        //throw new Exception()
    }
}